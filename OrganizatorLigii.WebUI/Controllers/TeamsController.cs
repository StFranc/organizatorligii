﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using OrganizatorLigii.Domain.Entities;
using NHSession = OrganizatorLigii.Domain.Session;

namespace OrganizatorLigii.WebUI.Controllers
{
    public class TeamsController : Controller
    {
        // GET: Teams
        public ActionResult Index()
        {
			IEnumerable<Team> teams = null;
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var query = session.Query<Team>();
				teams = query.ToList();
			}
            return View(teams);
        }

        // GET: Teams/Details/5
        public ActionResult Details(int teamId)
		{
			Team team = null;
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var query = session.Query<Team>()
					.Where(x => x.TeamId == teamId)
					.Fetch(x => x.Location)
					.FetchMany(x => x.AwayGames).ThenFetch(x => x.HomeTeam);

				team = query.FirstOrDefault();
				team.HomeGames = session.Query<Game>()
					.Where(x => x.HomeTeam.TeamId == team.TeamId)
					.Fetch(x => x.AwayTeam).ToList();
			}
			return View(team);
        }

        // GET: Teams/Create
        public ActionResult Create()
        {
			try
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var query = session.Query<Location>();
					ViewData["Locations"] = query.ToList();
				}
			}
			catch{ }
			return View();
        }

        // POST: Teams/Create
        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(Team team)
		{
			if (!ModelState.IsValid)
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					ViewData["Locations"] = session.Query<Location>().ToList();
					return View(team);
				}
			}
			try
			{
				byte[] crest = null;
				using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
				{
					crest = binaryReader.ReadBytes(Request.Files[0].ContentLength);
				}
				team.Crest = crest;
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var location = session.Query<Location>()
						.FirstOrDefault(l => l.LocationId == team.LocationId);
					team.Location = location;

					ViewData["Locations"] = session.Query<Location>().ToList();

					session.Save(team);
					transaction.Commit();
				}
				return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Teams/Edit/5
        public ActionResult Edit(int teamId)
		{
			Team team = null;
			try
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var query = session.Query<Team>()
						.Where(x => x.TeamId == teamId);
					var query2 = session.Query<Location>();
					ViewData["Locations"] = query2.ToList();
					team = query.FirstOrDefault();
				}
			}
			catch
			{
				return View();
			}
			return View(team);
        }

        // POST: Teams/Edit/5
        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TeamId, Name, FoundingDate, Description, LocationId")]Team team, int locationId)
		{
			if (!ModelState.IsValid)
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					ViewData["Locations"] = session.Query<Location>().ToList();
					return View(team);
				}
			}
			try
			{
				byte[] crest = null;
				using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
				{
					crest = binaryReader.ReadBytes(Request.Files[0].ContentLength);
				}
				team.Crest = crest;
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var dbTeam = session.Query<Team>()
						.FirstOrDefault(t => t.TeamId == team.TeamId);
					var location = session.Query<Location>()
						.FirstOrDefault(l => l.LocationId == locationId);

					dbTeam.Location = location;
					dbTeam.Crest = crest;
					dbTeam.Name = team.Name;
					dbTeam.Description = team.Description;
					dbTeam.FoundingDate = team.FoundingDate;

					//session.Update(team);
					transaction.Commit();
				}
			}
            catch(Exception ex)
			{
			}
			return RedirectToAction("Index");
		}

        // POST: Teams/Delete/5
        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Delete(int teamId, FormCollection collection)
        {
            try
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var team = session.Query<Team>()
						.Where(x => x.TeamId == teamId)
						.Delete();
					transaction.Commit();
				}
            }
            catch(Exception ex)
            {
			}
			return RedirectToAction("Index");
		}
	}
}
