﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate.Linq;
using OrganizatorLigii.Domain.Entities;
using NHSession = OrganizatorLigii.Domain.Session;

namespace OrganizatorLigii.WebUI.Controllers
{
	public class HomeController : Controller
	{

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		// Posts
		public ActionResult Index()
		{
			List<Post> posts = null;
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				posts = session.Query<Post>()
					.FetchMany(x => x.PostComments)
					.ToList();
				posts.Reverse();
				posts.ForEach(x => x.PostComments = x.PostComments.Reverse().ToList());
			}
			return View(posts);
		}

		public ActionResult Form(int? postId)
		{
			if (postId == null)
			{
				return View();
			}
			else
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var post = session.Query<Post>()
						.FirstOrDefault(x => x.PostId == postId);
					return View(post);
				}
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Write(Post post)
		{
			if (!ModelState.IsValid)
			{
				return View("Form", post);
			}
			else
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					if (post.PostId == null)
					{
						post.PostingDate = DateTime.Now;
						var admin = session.Query<User>()
							.FirstOrDefault(x => x.Login.Equals("admin"));
						post.User = admin;
						session.Save(post);
					}
					else
					{
						var dbPost = session.Query<Post>()
							.FirstOrDefault(x => x.PostId == post.PostId);
						dbPost.Text = post.Text;
						dbPost.Title = post.Title;
						session.Update(dbPost);
					}
					transaction.Commit();
				}
			}
			return RedirectToAction("Index");
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int postId)
		{
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var post = session.Query<Post>()
					.FirstOrDefault(x => x.PostId == postId);
				/*
				session.CreateQuery("DELETE FROM PostComment WHERE id_postu = :postId")
					.SetParameter("postId", post.PostId)
					.ExecuteUpdate();
				*/
				session.Delete(post);
				transaction.Commit();
			}
			return RedirectToAction("Index");
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult WriteComment(PostComment comment, int postId)
		{
			ActionResult result = null;
			if (!ModelState.IsValid)
			{
			
			}
			else
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var post = session.Query<Post>()
						.FirstOrDefault(x => x.PostId == postId);
					comment.Post = post;
					session.SaveOrUpdate(comment);
					transaction.Commit();
				}
			}
			result = RedirectToAction("Index");
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteComment(int commentId)
		{
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var comment = session.Query<PostComment>()
					.FirstOrDefault(x => x.PostCommentId == commentId);
				session.Delete(comment);
				transaction.Commit();
			}
			return RedirectToAction("Index");
		}
	}
}