﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate.Linq;
using OrganizatorLigii.Domain.Entities;
using NHSession = OrganizatorLigii.Domain.Session;

namespace OrganizatorLigii.WebUI.Controllers
{
    public class GamesController : Controller
    {
        // GET: Games/Details/5
        public ActionResult Details(int gameId)
		{
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var game = session.Query<Game>()
					.Fetch(x => x.AwayTeam)
					.Fetch(x => x.HomeTeam)
					.FirstOrDefault(x => x.GameId == gameId);
				game.GameComments = session.Query<GameComment>()
					.Where(x => x.Game.GameId == game.GameId)
					.ToList();
				return View(game);
			}

        }

        // GET: Games/Create
        public ActionResult Create(int teamId)
        {
			ViewData["TeamId"] = teamId;
			try
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var query = session.Query<Team>();
					ViewData["Teams"] = query.ToList();
				}
			}
			catch { }
			return View();
        }

        // POST: Games/Create
        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(Game game, int teamId, bool awayGame)
		{
			ViewData["TeamId"] = teamId;
			if (!ModelState.IsValid)
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var query = session.Query<Team>();
					ViewData["Teams"] = query.ToList();
					return View(game);
				}
			}
			else
			{
				try
				{
					using (var session = NHSession.OpenSession())
					using (var transaction = session.BeginTransaction())
					{
						var teams = session.Query<Team>().ToList();
						ViewData["Teams"] = teams;
						var myTeam = teams.FirstOrDefault(t => t.TeamId == teamId);
						var opponentTeam = teams.FirstOrDefault(t => t.TeamId == game.OpponentId);
						game.AwayTeam = (awayGame) ? myTeam : opponentTeam;
						game.HomeTeam = (awayGame) ? opponentTeam : myTeam;

						session.Save(myTeam);
						session.Save(opponentTeam);
						session.Save(game);
						transaction.Commit();
					}
				}
				catch (Exception ex)
				{
				}

				return RedirectToAction("Details", "Teams", new { teamId });
			}
		}

        // GET: Games/Edit/5
        public ActionResult Edit(int gameId)
        {
            return View();
        }

        // POST: Games/Edit/5
        [HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Game game)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: Games/Delete/5
        [HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int gameId, int teamId)
        {
            try
            {
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var game = session.Query<Game>()
						.Where(g => g.GameId == gameId)
						.FirstOrDefault();

					session.CreateQuery("DELETE FROM GameComment WHERE id_meczu = :gameId")
						.SetParameter("gameId", game.GameId)
						.ExecuteUpdate();
					session.Delete(game);
					transaction.Commit();
				}
            }
            catch
            {
			}
			return RedirectToAction("Details", "Teams", new { teamId });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult WriteComment(GameComment comment, int gameId)
		{
			ActionResult result = null;
			if (!ModelState.IsValid)
			{
			}
			else
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var game = session.Query<Game>()
						.FirstOrDefault(x => x.GameId == gameId);
					comment.Game = game;
					game.GameComments.Add(comment);
					session.SaveOrUpdate(comment);
					transaction.Commit();
				}
			}
			result = RedirectToAction("Details", new {  gameId });
			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteComment(int commentId)
		{
			ActionResult result = null;
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var comment = session.Query<GameComment>()
					.Fetch(x => x.Game)
					.FirstOrDefault(x => x.GameCommentId == commentId);
				int gameId = comment.Game.GameId;
				session.Delete(comment);
				transaction.Commit();
				result = RedirectToAction("Details", new {  gameId });
			}
			return result;
		}
	}
}
