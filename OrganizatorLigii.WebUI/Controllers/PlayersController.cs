﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate.Linq;
using OrganizatorLigii.Domain.Entities;
using NHSession = OrganizatorLigii.Domain.Session;

namespace OrganizatorLigii.WebUI.Controllers
{
    public class PlayersController : Controller
    {
        // GET: Players
        public ActionResult Index(int teamId)
		{
			IEnumerable<Player> players = null;
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				var query = session.Query<Player>()
					.Where(x => x.Team.TeamId == teamId);
				players = query.ToList();
			}
			ViewData["TeamId"] = teamId;
			return View(players);
		}

        // GET: Players/Create
        public ActionResult Create(int teamId)
		{
			ViewData["TeamId"] = teamId;
			return View();
        }

        // POST: Players/Create
        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Create(Player player, int teamId)
        {
			if (!ModelState.IsValid)
			{
				ViewData["TeamId"] = teamId;
				return View(player);
			}
            try
			{
				byte[] photo = null;
				using (var binaryReader = new BinaryReader(Request.Files[0].InputStream))
				{
					photo = binaryReader.ReadBytes(Request.Files[0].ContentLength);
				}
				player.Photo = photo;
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var team = session.Query<Team>()
						.FirstOrDefault(x => x.TeamId == teamId);
					team.TeamPlayers.Add(player);
					player.Team = team;

					session.Save(player);
					transaction.Commit();
				}
				return RedirectToAction("Details", "Teams", new { teamId });
            }
            catch(Exception ex)
			{
				ViewData["TeamId"] = teamId;
				return View();
            }
        }

        // POST: Players/Delete/5
        [HttpPost]
		[ValidateAntiForgeryToken]
        public ActionResult Delete(int playerId)
        {
			int? teamId = null;
            try
            {
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var player = session.Query<Player>()
						.Where(x => x.PlayerId == playerId)
						.Fetch(x => x.Team)
						.FirstOrDefault();
					teamId = player.Team.TeamId;
					session.Delete(player);
					transaction.Commit();
				}
			}
            catch { }
			return RedirectToAction("Details", "Teams", new { teamId });
		}
    }
}
