﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrganizatorLigii.Domain.Entities;
using NHSession = OrganizatorLigii.Domain.Session;

namespace OrganizatorLigii.WebUI.Controllers
{
    public class LocationsController : Controller
    {

        // GET: Locations
        public ActionResult Index()
        {
			ActionResult result = null;
			using (var session = NHSession.OpenSession())
			using (var transaction = session.BeginTransaction())
			{
				IEnumerable<Location> locations = session
					.Query<Location>()
					.ToList();
				result = View(locations);
			}

			return result;
		}

		public ActionResult Form(int? locationId)
		{
			ActionResult result = null;
			if(locationId != null)
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					var location = session
						.Query<Location>()
						.FirstOrDefault(x => x.LocationId == locationId);
					result = View(location);
				}
			}
			else
			{
				result = View();
			}

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Write(Location location)
		{
			ActionResult result = null;
			if (!ModelState.IsValid)
			{
				result = View("Form", location);
			}
			else
			{
				using (var session = NHSession.OpenSession())
				using (var transaction = session.BeginTransaction())
				{
					session.SaveOrUpdate(location);
					transaction.Commit();
					result = RedirectToAction("Index");
				}
			}

			return result;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Delete(int locationId)
		{
			ActionResult result = null;
			try{
				if (ModelState.IsValid)
				{
					using (var session = NHSession.OpenSession())
					using (var transaction = session.BeginTransaction())
					{
						var toDelete = session.Query<Location>()
							.FirstOrDefault(x => x.LocationId == locationId);
						session.Delete(toDelete);
						transaction.Commit();
					}
				}
			}
			catch(Exception ex)
			{
			}
			result = RedirectToAction("Index");
			return result;
		}
	}
}