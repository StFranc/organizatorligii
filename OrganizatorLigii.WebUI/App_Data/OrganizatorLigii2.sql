CREATE DATABASE [Liga]
GO

USE [Liga]
GO

CREATE TABLE [Miejsce] (
	[id_miejsca] INT IDENTITY(1,1) NOT NULL,
	[nazwa] NVARCHAR(100) NOT NULL,
	[miejscowosc] NVARCHAR(100) NOT NULL,

	CONSTRAINT [PK_Miejsce]
		PRIMARY KEY ([id_miejsca]),
)
GO

CREATE TABLE [Klub] (
	[id_klubu] INT IDENTITY(1,1) NOT NULL,
	[id_miejsca] INT NOT NULL,
	[nazwa] NVARCHAR(100) NOT NULL,
	[rok_zalozenia] DATE NOT NULL,
	[herb] VARBINARY(MAX),
	[opis] NVARCHAR(4000),

	CONSTRAINT [PK_Klub]
		PRIMARY KEY ([id_klubu]),
	CONSTRAINT [FK_Klub_Miejsce]
		FOREIGN KEY ([id_miejsca]) REFERENCES [Miejsce]([id_miejsca]),
)
GO

CREATE TABLE [Zawodnik] (
	[id_zawodnika] INT IDENTITY(1,1) NOT NULL,
	[id_klubu] INT NOT NULL,
	[imie] NVARCHAR(100) NOT NULL,
	[nazwisko] NVARCHAR(100) NOT NULL,
	[pozycja_boisko] NVARCHAR(50) NOT NULL,
	[nr_koszulki] INT NOT NULL,
	[zdjecie] VARBINARY(MAX),
	[opis_kariery] NVARCHAR(4000),

	CONSTRAINT [PK_Zawodnik]
		PRIMARY KEY ([id_zawodnika]),
	CONSTRAINT [FK_Zawodnik_Klub]
		FOREIGN KEY ([id_klubu]) REFERENCES [Klub]([id_klubu])
		ON DELETE CASCADE,
)
GO

CREATE TABLE [Konto] (
	[id_konta] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL,
	[id_klubu] INT NULL,
	[login] NVARCHAR(100) NOT NULL,
	[haslo] NVARCHAR(100) NOT NULL,

	CONSTRAINT [PK_Konto] 
		PRIMARY KEY ([id_konta]),
	CONSTRAINT [FK_Konto_Klub]
		FOREIGN KEY ([id_klubu]) REFERENCES [Klub]([id_klubu]),
)
GO

CREATE TABLE [Rola] (
	[id_roli] INT IDENTITY(1,1) NOT NULL,
	[nazwa] VARCHAR(50) NOT NULL,

	CONSTRAINT [PK_Rola]
		PRIMARY KEY ([id_roli]),
)
GO

CREATE TABLE [Konto_Rola] (
	[id_konta] UNIQUEIDENTIFIER NOT NULL,
	[id_roli] INT NOT NULL,

	CONSTRAINT [PK_Konto_Rola]
		PRIMARY KEY ([id_konta], [id_roli]),
	CONSTRAINT [FK_KontoRola_Konto]
		FOREIGN KEY ([id_konta]) REFERENCES [Konto]([id_konta])
		ON DELETE CASCADE,
	CONSTRAINT [FK_KontoRola_Rola]
		FOREIGN KEY ([id_roli]) REFERENCES [Rola]([id_roli])
		ON DELETE CASCADE,
)
GO

CREATE TABLE [Rezultat](
	[id_rezultatu] INT IDENTITY(1,1) NOT NULL,
	[nazwa] NVARCHAR(100) NOT NULL,

	CONSTRAINT [PK_Rezultat]
		PRIMARY KEY ([id_rezultatu]),
)
GO

CREATE TABLE [Mecz] (
	[id_meczu] INT IDENTITY(1,1) NOT NULL,
	[id_klubu_gosc] INT NOT NULL,
	[id_klubu_gospodarz] INT NOT NULL,
	[data] DATETIME2 DEFAULT GETDATE() NOT NULL,
	[ilosc_goli_gosc] INT, 
	[ilosc_goli_gospodarz] INT,
	[id_rezultatu] INT,

	CONSTRAINT [PK_Mecz]
		PRIMARY KEY ([id_meczu]),
	CONSTRAINT [FK_Mecz_KlubGosc]
		FOREIGN KEY ([id_klubu_gosc]) REFERENCES [Klub]([id_klubu]),
	CONSTRAINT [FK_Mecz_KlubGospodarz]
		FOREIGN KEY ([id_klubu_gospodarz]) REFERENCES [Klub]([id_klubu]),
	CONSTRAINT [FK_Mecz_Rezultat]
		FOREIGN KEY ([id_rezultatu]) REFERENCES [Rezultat]([id_rezultatu]),
)
GO

CREATE TABLE [Komentarz_Mecz] (
	[id_komentarza] INT IDENTITY(1,1) NOT NULL,
	[id_meczu] INT NOT NULL,
	[tekst] NVARCHAR(1000) NOT NULL,
	[data] DATETIME2 DEFAULT GETDATE() NOT NULL,

	CONSTRAINT [PK_KomentarzMeczu]
		PRIMARY KEY ([id_komentarza]),
	CONSTRAINT [FK_Komentarz_Mecz]
		FOREIGN KEY ([id_meczu]) REFERENCES [Mecz]([id_meczu])
		ON DELETE CASCADE,
)
GO

CREATE TABLE [Post](
	[id_postu] INT IDENTITY(1,1) NOT NULL,
	[id_konta] UNIQUEIDENTIFIER NULL,
	[tytul] NVARCHAR(MAX) NOT NULL,
	[tekst] NVARCHAR(MAX) NOT NULL,
	[data] DATETIME2  DEFAULT GETDATE() NOT NULL,

	CONSTRAINT [PK_Post]
		PRIMARY KEY ([id_postu]),
	CONSTRAINT [FK_Post_Konto]
		FOREIGN KEY ([id_konta]) REFERENCES [Konto]([id_konta]),
)
GO

CREATE TABLE [Komentarz_Post](
	[id_komentarza] INT IDENTITY(1,1) NOT NULL,
	[id_postu] INT NOT NULL,
	[tekst] NVARCHAR(1000) NOT NULL,
	[data] DATETIME2 DEFAULT GETDATE() NOT NULL,

	CONSTRAINT [PK_KomentarzPostu]
		PRIMARY KEY ([id_komentarza]),
	CONSTRAINT [FK_Komentarz_Post]
		FOREIGN KEY ([id_postu]) REFERENCES [Post]([id_postu])
		ON DELETE CASCADE,
)
GO

CREATE TRIGGER miejsce_usuwanie_kaskadowe
ON [Miejsce]
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [Klub] WHERE [id_miejsca] IN (SELECT [id_miejsca] FROM deleted)
	DELETE FROM [Miejsce] WHERE [id_miejsca] IN (SELECT [id_miejsca] FROM deleted)
END
GO

CREATE TRIGGER klub_usuwanie_kaskadowe
ON [Klub] 
INSTEAD OF DELETE
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [Mecz]
		WHERE [id_klubu_gospodarz] IN (SELECT [id_klubu] FROM deleted) 
		OR [id_klubu_gosc] IN (SELECT [id_klubu] FROM deleted)
	DELETE FROM [Klub] WHERE [id_klubu] IN (SELECT [id_klubu] FROM deleted)
END
GO


INSERT INTO [Konto] ([login], [haslo])
VALUES ('admin', 'admin');