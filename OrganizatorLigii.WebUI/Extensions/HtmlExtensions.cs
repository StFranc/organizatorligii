﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrganizatorLigii.WebUI.Extensions
{
	public static class HtmlExtensions
	{
		public static MvcHtmlString Image(this HtmlHelper html,
			byte[] image, string htmlClass = "", string style = "", string onErrorPath = "")
		{
			var img = String.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(image));
			return new MvcHtmlString("<img src='" + img +
				"' onerror='this.src=\"" + onErrorPath +
				"\";' class='" + htmlClass +
				"' style='" + style +
				"'></img>");
		}
	}
}