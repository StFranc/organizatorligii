﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class UserMap: ClassMap<User>
	{
		public UserMap()
		{
			Table("Konto");
			Id(x => x.UserId).Column("id_konta");
			HasManyToMany(x => x.UserRoles)
				.Table("Konto_Rola")
				.ParentKeyColumn("id_konta")
				.ChildKeyColumn("id_roli")
				.Cascade.All();
			Map(x => x.Password).Column("haslo");
			Map(x => x.Login).Column("login");
		}
	}
}
