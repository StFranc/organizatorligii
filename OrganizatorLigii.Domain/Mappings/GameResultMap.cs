﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class GameResultMap: ClassMap<GameResult>
	{
		public GameResultMap()
		{
			Table("Rezultat");
			Id(x => x.GameResultId).Column("id_rezultatu");
			Map(x => x.Name).Column("nazwa");
		}
	}
}
