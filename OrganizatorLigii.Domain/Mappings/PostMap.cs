﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class PostMap: ClassMap<Post>
	{
		public PostMap()
		{
			Table("Post");
			Id(x => x.PostId).Column("id_postu")
				.GeneratedBy.Identity();
			References(x => x.User).Column("id_konta");
			Map(x => x.PostingDate).Column("data");
			Map(x => x.Title).Column("tytul");
			Map(x => x.Text).Column("tekst");
			HasMany(x => x.PostComments).KeyColumn("id_postu")
				.Inverse()
				.Cascade.AllDeleteOrphan();
		}
	}
}
