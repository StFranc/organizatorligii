﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class TeamMap: ClassMap<Team>
	{
		public TeamMap()
		{
			Table("Klub");
			Id(x => x.TeamId).Column("id_klubu");
			References(x => x.Location).Column("id_miejsca");
			Map(x => x.Name).Column("nazwa");
			Map(x => x.FoundingDate).Column("rok_zalozenia");
			Map(x => x.Crest).Column("herb").Length(Int32.MaxValue);
			Map(x => x.Description).Column("opis");
			HasMany(x => x.TeamPlayers).KeyColumn("id_klubu");
			HasMany(x => x.HomeGames).KeyColumn("id_klubu_gospodarz");
			HasMany(x => x.AwayGames).KeyColumn("id_klubu_gosc");
			HasMany(x => x.Users).KeyColumn("id_klubu");
		}
	}
}
