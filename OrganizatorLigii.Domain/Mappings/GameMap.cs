﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class GameMap : ClassMap<Game>
	{
		public GameMap()
		{
			Table("Mecz");
			Id(x => x.GameId).Column("id_meczu");
			References(x => x.AwayTeam).Column("id_klubu_gosc")
				.Cascade.Delete();
			References(x => x.HomeTeam).Column("id_klubu_gospodarz")
				.Cascade.Delete();
			References(x => x.GameResult).Column("id_rezultatu");
			HasMany(x => x.GameComments).KeyColumn("id_meczu")
				.Inverse()
				.Cascade.AllDeleteOrphan();
			Map(x => x.GameDate).Column("data");
			Map(x => x.HomeTeamScore).Column("ilosc_goli_gospodarz");
			Map(x => x.AwayTeamScore).Column("ilosc_goli_gosc");
		}
	}
}
