﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class PostCommentMap: ClassMap<PostComment>
	{
		public PostCommentMap()
		{
			Table("Komentarz_Post");
			Id(x => x.PostCommentId).Column("id_komentarza")
				.GeneratedBy.Identity();
			References(x => x.Post).Column("id_postu");
				/*.Not.Update()
				.Not.Nullable();*/

			Map(x => x.Text).Column("tekst");
			Map(x => x.PostingDate).Column("data");
		}
	}
}
