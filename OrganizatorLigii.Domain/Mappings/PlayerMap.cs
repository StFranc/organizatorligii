﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class PlayerMap: ClassMap<Player>
	{
		public PlayerMap()
		{
			Table("Zawodnik");
			Id(x => x.PlayerId).Column("id_zawodnika");
			References(x => x.Team).Column("id_klubu");
			Map(x => x.FirstName).Column("imie");
			Map(x => x.LastName).Column("nazwisko");
			Map(x => x.JerseyNumber).Column("nr_koszulki");
			Map(x => x.Position).Column("pozycja_boisko");
			Map(x => x.Photo).Column("zdjecie").Length(Int32.MaxValue);
			Map(x => x.Description).Column("opis_kariery");
		}
	}
}
