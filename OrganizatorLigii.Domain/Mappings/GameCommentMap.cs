﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class GameCommentMap: ClassMap<GameComment>
	{
		public GameCommentMap()
		{
			Table("Komentarz_Mecz");
			Id(x => x.GameCommentId).Column("id_komentarza")
				.GeneratedBy.Identity();
			References(x => x.Game).Column("id_meczu");
			Map(x => x.Text).Column("tekst");
			Map(x => x.PostingDate).Column("data");
		}
	}
}
