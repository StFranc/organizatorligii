﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class LocationMap: ClassMap<Location>
	{
		public LocationMap()
		{
			Table("Miejsce");
			Id(x => x.LocationId).Column("id_miejsca")
				.GeneratedBy.Identity();
			Map(x => x.City).Column("miejscowosc");
			Map(x => x.Name).Column("nazwa");
		}
	}
}
