﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using OrganizatorLigii.Domain.Entities;

namespace OrganizatorLigii.Domain.Mappings
{
	class RoleMap: ClassMap<Role>
	{
		public RoleMap()
		{
			Table("Rola");
			Id(x => x.RoleId).Column("id_roli");
			HasManyToMany(x => x.UsersInRole)
				.Table("Konto_Rola")
				.ParentKeyColumn("id_roli")
				.ChildKeyColumn("id_konta")
				.Inverse()
				.Cascade.All();
			Map(x => x.RoleName).Column("nazwa");
		}
	}
}
