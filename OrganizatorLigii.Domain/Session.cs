﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using OrganizatorLigii.Domain.Entities;
using OrganizatorLigii.Domain.Mappings;

namespace OrganizatorLigii.Domain
{
	public class Session
	{
		public static ISession OpenSession()
		{
			var configuration = Fluently.Configure()
				.Database(MsSqlConfiguration.MsSql2012
					.ConnectionString(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Liga.mdf;Integrated Security=True"))
				.Mappings(m => {
					m.FluentMappings.AddFromAssemblyOf<GameCommentMap>();
				});

			var sessionFactory = configuration.BuildSessionFactory();
			return sessionFactory.OpenSession();
		}
	}
}
