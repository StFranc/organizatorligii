﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class Player
	{
		public virtual int PlayerId { get; set; }

		public virtual Team Team { get; set; }

		[Display(Name = "Imię")]
		[Required(ErrorMessage = "Pole Wymagane")]
		public virtual string FirstName { get; set; }

		[Display(Name = "Nazwisko")]
		[Required(ErrorMessage = "Pole Wymagane")]
		public virtual string LastName { get; set; }

		[Display(Name = "Pozycja")]
		[Required(ErrorMessage = "Pole Wymagane")]
		public virtual string Position { get; set; }

		[Display(Name = "Numer Koszulki")]
		[Required(ErrorMessage = "Pole Wymagane")]
		public virtual int JerseyNumber { get; set; }

		[Display(Name = "Zdjęcie")]
		public virtual byte[] Photo { get; set; }

		[Display(Name = "Opis Kariery")]
		[Required(ErrorMessage = "Pole Wymagane")]
		public virtual string Description { get; set; }
	}
}
