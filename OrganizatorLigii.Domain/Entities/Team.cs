﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class Team
	{
		public virtual int TeamId { get; set; }

		public virtual Location Location { get; set; }

		[Display(Name = "Nazwa Klubu")]
		[Required(ErrorMessage ="Pole wymagane")]
		public virtual string Name { get; set; }

		[Display(Name = "Data Założenia")]
		[Required(ErrorMessage = "Pole wymagane")]
		public virtual DateTime FoundingDate { get; set; }

		[Display(Name = "Herb")]
		public virtual byte[] Crest { get; set; }

		[Display(Name = "Opis")]
		[Required(ErrorMessage = "Pole wymagane")]
		public virtual string Description { get; set; }

		[Display(Name = "Lokacja")]
		[Required(ErrorMessage = "Pole wymagane")]
		public virtual int? LocationId { get; set; }

		public virtual IList<User> Users { get; set; }

		public virtual IList<Game> HomeGames { get; set; }

		public virtual IList<Game> AwayGames { get; set; }

		public virtual IList<Player> TeamPlayers { get; set; }

		public Team()
		{
			Users = new List<User>();
			HomeGames = new List<Game>();
			AwayGames = new List<Game>();
			TeamPlayers = new List<Player>();
		}
	}
}
