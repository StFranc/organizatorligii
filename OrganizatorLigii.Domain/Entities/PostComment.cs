﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class PostComment
	{
		public virtual int PostCommentId { get; set; }

		public virtual Post Post { get; set; }

		[Required]
		[Display(Name = "Zawartość")]
		public virtual string Text { get; set; }

		public virtual DateTime PostingDate { get; set; }
	}
}
