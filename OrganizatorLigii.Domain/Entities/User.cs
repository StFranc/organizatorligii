﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class User
	{
		public virtual string UserId { get; set; }

		public virtual string Login { get; set; }

		public virtual string Password { get; set; }
		
		public virtual IList<Role> UserRoles { get; set; }

		public virtual Team Team { get; set; }

		public User()
		{
			UserRoles = new List<Role>();
		}
	}
}
