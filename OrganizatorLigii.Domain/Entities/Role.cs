﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class Role
	{
		public virtual int RoleId { get; set; }

		public virtual string RoleName { get; set; }

		public virtual IList<User> UsersInRole { get; set; }

		public Role()
		{
			UsersInRole = new List<User>();
		}
	}
}
