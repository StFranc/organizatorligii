﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class Post
	{
		public virtual int? PostId { get; set; }

		public virtual User User { get; set; }

		[Required]
		[Display(Name = "Tytuł")]
		public virtual string Title { get; set; }

		[Required]
		[Display(Name = "Zawartość")]
		public virtual string Text { get; set; }
		
		public virtual DateTime PostingDate { get; set; }

		public virtual IList<PostComment> PostComments { get; set; }

		public Post()
		{
			PostComments = new List<PostComment>();
		}
	}
}
