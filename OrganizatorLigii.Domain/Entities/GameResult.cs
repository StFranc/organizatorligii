﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class GameResult
	{
		public virtual int GameResultId { get; set; }

		[Required]
		[Display(Name = "Nazwa")]
		public virtual string Name { get; set; }
	}
}
