﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class Game
	{
		public virtual int GameId { get; set; }

		public virtual GameResult GameResult { get; set; }
		
		public virtual Team AwayTeam { get; set; }
		
		public virtual Team HomeTeam { get; set; }

		[Display(Name = "Data Meczu")]
		[Required]
		public virtual DateTime GameDate { get; set; }

		[Display(Name = "Bramki Gości")]
		[Required]
		public virtual int AwayTeamScore { get; set; }

		[Display(Name = "Bramki Gospodarzy")]
		[Required]
		public virtual int HomeTeamScore { get; set; }

		[Display(Name = "Rywal")]
		[Required]
		public virtual int? OpponentId { get; set; }

		public virtual IList<GameComment> GameComments { get; set; }

		public Game()
		{
			GameComments = new List<GameComment>();
		}
	}
}
