﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrganizatorLigii.Domain.Entities
{
	public class Location
	{
		public virtual int? LocationId { get; set; }

		[Required(ErrorMessage = "Pole Wymagane.")]
		[Display(Name = "Miasto")]
		public virtual string City { get; set; }

		[Required(ErrorMessage = "Pole Wymagane.")]
		[Display(Name ="Nazwa")]
		public virtual string Name { get; set; }
	}
}
